package com.classpathio.orders.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class AppSecurityConfig {
	
	/*
	 * @Autowired private DomainUserDetailsService domainUserDetailsService;
	 */
	
	//Authorization
	/**
	 * Build the filter chain
	 * This is for Authorization
	 */
	
	@Bean
	public SecurityFilterChain configure(HttpSecurity http) throws Exception{
		http.cors().disable();
		http.csrf().disable();
		http.headers().frameOptions().disable();
		http.authorizeRequests()
			.antMatchers("login**", "/actuator/**", "/h2-console**", "/h2-console/**")
				.permitAll()
			.antMatchers(HttpMethod.GET, "/api/orders/**")
				.hasAnyRole("Everyone", "super_admins","admins")
			.antMatchers(HttpMethod.POST, "/api/orders/**")
				.hasAnyRole("super_admins","admins")
			.antMatchers(HttpMethod.DELETE, "/api/orders/**")
				.hasRole("super_admins")
			.anyRequest()
				.authenticated()
			.and()
				.oauth2ResourceServer()
				.jwt();
		return http.build();
	}
	//Adaptor which converts the JWT claims to Apring Security Roles
	@Bean
	public JwtAuthenticationConverter jwtAuthenticationConveret() {
		JwtAuthenticationConverter jwtAuthConverter = new JwtAuthenticationConverter();
		JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
		jwtGrantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");
		jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName("groups");
		jwtAuthConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
		return jwtAuthConverter;
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	/*
	 * @Override protected void configure(AuthenticationManagerBuilder auth) throws
	 * Exception { auth .userDetailsService(this.domainUserDetailsService)
	 * .passwordEncoder(this.passwordEncoder()); }
	 */
}
