package com.classpathio.orders.util;

import java.util.Arrays;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordUtil {
	
	public static void main(String[] args) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		String rawString = "welcome";
		
		String encodedPassword1 = passwordEncoder.encode(rawString);
		String encodedPassword2 = passwordEncoder.encode(rawString);
		String encodedPassword3 = passwordEncoder.encode(rawString);
		String encodedPassword4 = passwordEncoder.encode(rawString);
		
		
		
		Arrays.asList(encodedPassword1, encodedPassword2, encodedPassword3,encodedPassword4)
				.stream().forEach(System.out::println);
		
		System.out.println(passwordEncoder.matches(rawString, encodedPassword1));
		
	}

}
