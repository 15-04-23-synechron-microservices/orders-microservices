package com.classpathio.orders.util;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import com.classpathio.orders.model.Role;
import com.classpathio.orders.model.User;
import com.classpathio.orders.repository.UserRepository;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
class BootstrapAppData {
	
	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	
	@Transactional
	@EventListener(ApplicationReadyEvent.class)
	public void handleApplicationReady(ApplicationReadyEvent event) {

		Role userRole = new Role();
		userRole.setRole("ROLE_USER");
		Role adminRole = new Role();
		adminRole.setRole("ROLE_ADMIN");
		User kiran = new User();
		kiran.setUsername("kiran");
		kiran.setPassword(passwordEncoder.encode("welcome"));
		kiran.setEmail("kiran@gmail.com");
		this.userRepository.save(kiran);
		kiran.addRole(userRole);
		User vinay = new User();
		vinay.setUsername("vinay");
		vinay.setEmail("vinay@gmail.com");
		vinay.setPassword(passwordEncoder.encode("welcome"));
		this.userRepository.save(vinay);
		vinay.addRole(userRole);
		vinay.addRole(adminRole);
	}
}	
