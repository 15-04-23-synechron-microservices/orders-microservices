package com.classpathio.orders.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.classpathio.orders.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{

}
