package com.classpathio.orders.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.classpathio.orders.model.Order;
import com.classpathio.orders.service.OrderService;

@RestController
@RequestMapping("/api/orders")
public class OrderRestController {

	@Autowired
	private OrderService orderService;

	@GetMapping
	public Map<String, Object> fetchAllOrders(
			@RequestParam(name = "page", defaultValue = "0", required = false) int  page, 
			@RequestParam(name = "size", defaultValue = "10", required = false) int size) {
		
		return this.orderService.fetchAllOrders(page, size);
	}

	@GetMapping("/{id}")
	public Order fetchOrderById(@PathVariable("id") long id) {
		return this.orderService.fetchOrderById(id);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Order saveOrder(@RequestBody Order order) {
		return this.orderService.saveOrder(order);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteOrderById(@PathVariable("id") long id) {
		this.orderService.deleteOrderById(id);
	}

	@PutMapping("/{id}")
	public Order updateExistingOrder(@PathVariable("id") long id, @RequestBody Order updatedOrder) {
		return this.orderService.updateOrder(id, updatedOrder);
	}

}
