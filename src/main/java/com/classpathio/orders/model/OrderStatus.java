package com.classpathio.orders.model;

public enum OrderStatus {
	
	ORDER_PLACED,
	
	ORDER_FULFILLED,
	
	ORDER_CANCELLED

}
