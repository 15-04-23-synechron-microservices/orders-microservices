package com.classpathio.orders.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.classpathio.orders.model.DomainUserDetails;
import com.classpathio.orders.model.User;
import com.classpathio.orders.repository.UserRepository;

@Service
public class DomainUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return this.userRepository
								.findByUsername(username)
								.map(DomainUserDetails::new)
								.orElseThrow(() -> new UsernameNotFoundException("invalid username"));
	}

}
