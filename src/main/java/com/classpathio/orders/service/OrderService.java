package com.classpathio.orders.service;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import com.classpathio.orders.model.LineItem;
import com.classpathio.orders.model.Order;
import com.classpathio.orders.model.OrderEvent;
import com.classpathio.orders.model.OrderStatus;
import com.classpathio.orders.repository.OrderRepository;

import io.github.resilience4j.retry.annotation.Retry;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private WebClient webClient;
	
	@Autowired
	private StreamBridge streamBridge;
	
	//@CircuitBreaker(name="inventoryservice", fallbackMethod = "fallBack")
	@Retry(name="inventoryServiceRetry", fallbackMethod = "fallBack")
	@Transactional
	public Order saveOrder(Order order) {
		Order savedOrder = this.orderRepository.save(order);
		OrderEvent orderPlacedEvent = OrderEvent
										.builder()
											.order(savedOrder)
											.status(OrderStatus.ORDER_PLACED)
											.timestamp(LocalDateTime.now())
										.build();
		
		Message<OrderEvent> orderEvent = MessageBuilder.withPayload(orderPlacedEvent).build();
		this.streamBridge.send("producer-out-0", orderEvent);
		
		/*
		 * System.out.println("Inside the save method response ::"); int integerResponse
		 * = this.webClient.post().uri("/api/inventory").exchangeToMono(response -> {
		 * return response.bodyToMono(Integer.class); }).block();
		 * System.out.println("Response from inventory microservice :: "+
		 * integerResponse);
		 */
		return savedOrder;
	}
	
	private Order fallBack(Throwable exception) {
		System.out.println("Exception :: "+ exception.getMessage());
		return Order.builder().build();
	}
	

	public Map<String, Object> fetchAllOrders(int page, int size) {
		
		PageRequest pageRequest = PageRequest.of(page, size);
		
		Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);
		
		long totalElements = pageResponse.getTotalElements();
		int totalPages = pageResponse.getTotalPages();
		int noOrRecords = pageResponse.getNumberOfElements();
		List<Order> content = pageResponse.getContent();
		
		
		Map<String, Object> responseMap = new LinkedHashMap<>();
		
		responseMap.put("pages", totalPages);
		responseMap.put("records", noOrRecords);
		responseMap.put("data", content);
		
		return responseMap;
	}

	public Order fetchOrderById(long id) {
		return this.orderRepository
						.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("invalid order id"));
	}

	public void deleteOrderById(long id) {
		this.orderRepository.deleteById(id);

	}

	public Order updateOrder(long orderId, Order updatedOrder) {
		/*
		 * first fetch the order by orderId.
		 * If the order is present, then update the order with the updated order data
		 * persist the changes to the db
		 */
		Order existingOrder = this.orderRepository
										.findById(orderId)
										.orElseThrow(() -> new EntityNotFoundException("order is not present"));
		//existingOrder.setCustomer(updatedOrder.getCustomer());
		Set<LineItem> existingLineItems = existingOrder.getLineItems();
		Set<LineItem> updatedLineItems = updatedOrder.getLineItems();
		
		//remove any lineItems that are not present in the updateLineItems
		existingLineItems.removeIf(lineItem -> !updatedLineItems.contains(lineItem));
		
		//Add any new LineItems to the existing order
		updatedLineItems.stream()
			.filter(lineItem -> !existingLineItems.contains(lineItem))
			.forEach( lineItem -> {
				//set both sides of the relationship
				existingOrder.addLineItem(lineItem);
			});
		return this.orderRepository.save(existingOrder);
	}


}
